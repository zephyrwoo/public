# 仁宝内结构

## 文件用途及路径

### 本地
仁宝数据基本都在G盘，其中仁宝第二批数据保存在`'G:\0710-0721数据标注\第二批标注完成'`中，分产线分型号保存。

`'G:\0710-0721数据标注\物料汇总(已发送)'`目录下的connector、PIN、插口等文件夹为第二批数据分物料汇总，可用于分物料训练模型。

### 服务器端(四卡GPU)

仁宝模型相关文件夹在`'/home/wangcheng/wuzhefu/classification_7.7'`和`'/home/wangcheng/wuzhefu/meta_classification'`，对应的训练数据集统一放在`'/home/wangcheng/wuzhefu/train_set/xxxxx(根据数据集命名)/mytestset'`

#### · 其中，`'/home/wangcheng/wuzhefu/classification_7.7'`对应训练指令实例为：

```
python3 main_all.py --class_num 3 \
--data /home/wangcheng/wuzhefu/train_set/PIN2/data/mytestset \
--save /home/wangcheng/wuzhefu/train_set/PIN2/data/save_50 \
--env PIN2 \
--pkl_name /home/wangcheng/wuzhefu/train_set/PIN2/data/save_50/best.pkl \
--gpu_num 0 \
--max_epoch 50 \
--gpu_num 2
```
对应的训练生成的模型在对应数据集文件夹中，如`'/home/wangcheng/wuzhefu/train_set/pincha/data/save'`

#### · `'/home/wangcheng/wuzhefu/meta_classification'`对应训练指令示例为：

```
python3 main.py --root /home/wangcheng/wuzhefu/train_set/P1P2_PIN/data \
--optim adam \
--batchsize 256 \
--decay linear \
--max_epoch 300 \
--seed 3000
```
对应的训练生成的模型在`'/home/wangcheng/wuzhefu/meta_classification/based_mata-learning'`或`'/home/wangcheng/wuzhefu/meta_classification/normal'`目录下（根据训练时的优化策略来选择）


# 外观检测

## 文件用途及路径

### 本地
外观检测相关文件基本都放在F盘，其中`'./total'`内保存的是第一批到第三批数据混合后按照点状、片状、条状针对不同瑕疵类型进行整合后的数据，分别保存在`'./total/dian'`、`'./total/pian'`、`'./total/tiao'`文件夹下。

### 服务器端(四卡GPU)

服务器端的分割模型保存在`'/home/wangcheng/wuzhefu/SegDecNet-wzf'`目录下，数据集放在`'/home/wangcheng/wuzhefu/SegDecNet-wzf/Data/xxx(根据数据集设定文件夹名)'`目录下，数据集存放目录结构可参照`'/home/wangcheng/wuzhefu/SegDecNet-wzf/Data/dian'`下的文件结构。训练文件为`train_seg2.py`和`train_dec2.py`，测试文件为`multigpu_test.py`
训练示例：

```
python3 train_seg2.py \
--gpu_num 2 --batch_size 8 \
--root ./Data/dian --end_epoch 1001 \
--dilate 5 --img_cls bmp --lbl_cls png \
--trainokfolder ng --traintestfolder test \
--trainokfolder test
```

测试示例：

```
python3 multigpu_test.py --root ./Data/one_plus_two \
--num_train_gpu 2 --segmodel ./one_plus_two_dilate_5_saved_model \
--decmodel ./saved_models --test_seg_epoch 50 --test_dec_epoch 100
```
其他设置参数见源文件

该模型每隔固定epoch输出一次验证集测试结果，测试结果会显示在`'/home/wangcheng/wuzhefu/SegDecNet-wzf'`目录下，文件夹命名与数据集所在的文件夹名有关，故不同数据集的测试结果不会相互覆盖。测试文件夹名为`data_(数据集文件夹名)_dilate_(膨胀系数)_segRST || data_(数据集文件夹名)_dilate_(膨胀系数)_DecRST`，模型保存路径在`'/home/wangcheng/wuzhefu/SegDecNet-wzf/(数据集文件夹名)_dilate_(膨胀系数)_saved_model'`

### 数据预处理
数据集中的原图为bmp格式，标签为json格式，需将json转化为mask，在`json2png.py`文件中修改`line12&13`的json_file和save_file参数后运行即可。
原题尺寸过大，瑕疵面积过小难以实现分割，故需先将原图裁剪后再进行训练，在`img_aug.py`中修改最后四行`line219~222`分别为原图路径、image patch路径、label路径、label_patch路径。如需修改数据增强的其他需求可以在`line146`修改opslist参数。
相关文件在[广达外观缺陷检测相关文件](https://gitee.com/zephyrwoo/public/tree/master/%E5%B9%BF%E8%BE%BE%E5%A4%96%E8%A7%82%E7%BC%BA%E9%99%B7%E6%A3%80%E6%B5%8B%E7%9B%B8%E5%85%B3%E6%96%87%E4%BB%B6)下